const http = require('http');

const port = 4000;

//3. Create a mock database with the data:
let database = [
	{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
	}
]

const server = http.createServer((req, res) => {

//4. Create a route "/profile" and request all the information in the data

	if(req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'})
		res.write(JSON.stringify(database));
		res.end()
	}

//5. Create a route for creating a new item upon receiving a POST request

	if(req.url == 'profile' && req.method == "POST"){
		let requestBody = '';

		req.on('data', function(data){
			requestBody += data;
		});

		req.on('end', function(){
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody)

			let newUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}

			database.push(newUser);
			console.log(database)

			res writeHead(200, {'Content-Type': 'application/json'})
			res.write(JSON.stringify(newUser));
			res.end()
		})
	}


server.listen(port);

console.log(`Server running at localhost:${port}`);











